<%@ page contentType="text/html; charset=utf-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
<title>用户登录</title>
<meta content='text/html; charset=utf-8' http-equiv='Content-Type' />
<link rel="shortcut icon" href="/springdemo/image/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="/springdemo/css/login.css" type="text/css" />
<script type="text/javascript" src="/springdemo/js/jquery/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/springdemo/js/jquery.placeholder.js"></script>
<script type="text/javascript">

</script>
</head>
<body>
	<div class="login">
		<form action="/springdemo/tologin" id="login-form" method="post">
			<h2>登录</h2>
			<div class="item">
			<input type="text" name="username" class="input-login" placeholder="用户名" />
			</div>
			<div class="item">
				<input type="password" name="password" class="input-login" placeholder="密码" />
			</div>
			<div class="item">
				<input type="submit" class="btn btn-login" id="login" value="登录" onclick="toggle();return true;"/> 
			</div>
		</form>
	</div>
</body>
</html>
