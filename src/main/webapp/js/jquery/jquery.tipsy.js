// tipsy, facebook style tooltips for jquery
// version 1.0.0a
// (c) 2008-2010 jason frame [jason@onehackoranother.com]
// released under the MIT license

(function($) {
    
    function maybeCall(thing, ctx) {
        return (typeof thing == 'function') ? (thing.call(ctx)) : thing;
    };
    
    function isElementInDOM(ele) {
      while (ele = ele.parentNode) {
        if (ele == document) return true;
      }
      return false;
    };
    
    function Tipsy(element, options) {
        this.$element = $(element);
        this.options = options;
        this.enabled = true;
        this.fixTitle();
        this.resetInfo = this.options.fallback;
    };
    
    Tipsy.prototype = {
        // added by wangw
        hasError: function(){
            if(this.tip().find(".tipsy-inner").hasClass("tipsy-inner-error")){
                return true;
            }else{
                return false;
            }
        },
        isTitleChanged: function(title){
            var old = this.options.fallback;
            if(old == title){
                return false;
            }else{
                return true;
            }
        },
    	changeTitle: function(title){
    		this.options.fallback=title;
//            this.show();
    	},
        reset: function(){
            this.tip().find(".tipsy-inner").removeClass("tipsy-inner-error");
            this.rebindMouseEvent()
            if(this.options.fallback != this.resetInfo){
                this.options.fallback = this.resetInfo;
                this.show();
            }
        },
        error: function(title) {
            this.tip().find(".tipsy-inner").addClass("tipsy-inner-error");
            this.unbindMouseEvent();
            this.options.fallback=title;
            this.show();
        },
        getPosition: function(){
            var $tip = this.tip();
            // 删除上一次提示的长度和高度
            $tip.css({"height": "auto", "width": "auto"});
            if(this.options.parentNode){
                var elemPos = this.$element.position()
            }else{
                var elemPos = this.$element.offset()
            }
            var pos = $.extend({}, elemPos, {
                width: this.$element[0].offsetWidth,
                height: this.$element[0].offsetHeight
            });
            var actualWidth = $tip[0].offsetWidth,
            actualHeight = $tip[0].offsetHeight,
            gravity = maybeCall(this.options.gravity, this.$element[0]);

            var tp;
            switch (gravity.charAt(0)) {
                case 'n':
                tp = {top: pos.top + pos.height + this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                break;
                case 's':
                tp = {top: pos.top - actualHeight - this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                break;
                case 'e':
                tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth - this.options.offset};
                break;
                case 'w':
                tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width + this.options.offset};
                break;
            }

            if (gravity.length == 2) {
                if (gravity.charAt(1) == 'w') {
                    tp.left = pos.left + actualWidth;
                } else {
                    tp.left = pos.left + pos.width - actualWidth;
                }
            } 
            if(this.options.parentNode){
                tp = $.extend(tp, {"z-index": "99"});
            } 
            return tp;
        },
        adjustPosition: function(){
            var $tip = this.tip();
            var tp = this.getPosition(); 
            $tip.css(tp)
        },
        rebindMouseEvent: function(){
            var options = this.options;
            if (options.trigger != 'manual') {
                var eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur',
                    eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus';
                if(options.live){
                    this.$element.live(eventOut, this.mouseOut).live(eventIn, this.mouseIn);
                }else{
                    this.$element.bind(eventOut, this.mouseOut).bind(eventIn, this.mouseIn);
                }
            }
        },
        unbindMouseEvent: function(){
            var options = this.options;
            if (options.trigger != 'manual') {
                var eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur',
                    eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus';
                if(options.live){
                    this.$element.die(eventOut).die(eventIn);
                }else{
                    this.$element.unbind(eventOut).unbind(eventIn);
                }
            }
        },
        mouseIn: function(){
            var tipsy = $.data(this, "tipsy");
            tipsy.hoverState = 'in';
            if (tipsy.options.delayIn == 0) {
                tipsy.show();
            } else {
                tipsy.fixTitle();
                setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, tipsy.options.delayIn);
            }
        },
        mouseOut: function(){
            var tipsy = $.data(this, "tipsy");
            tipsy.hoverState = 'out';
            if (tipsy.options.delayOut == 0) {
                tipsy.hide();
            } else {
                setTimeout(function() { if (tipsy.hoverState == 'out') tipsy.hide(); }, tipsy.options.delayOut);
            }
        },

        show: function() {
            var title = this.getTitle();
            if (title && this.enabled) {
                var $tip = this.tip();
                
                $tip.find('.tipsy-inner')[this.options.html ? 'html' : 'text'](title);
                $tip[0].className = 'tipsy'; // reset classname in case of dynamic gravity
                var $tipElem = $tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'});
                if(this.options.parentNode){
                    $tipElem.prependTo(this.$element.parent(this.options.parentNode));
                }else{
                    $tipElem.prependTo(document.body);
                }
                
                var tp = this.getPosition(),
                    gravity = maybeCall(this.options.gravity, this.$element[0]);
                $tip.css(tp).addClass('tipsy-' + gravity);
                $tip.find('.tipsy-arrow')[0].className = 'tipsy-arrow tipsy-arrow-' + gravity.charAt(0);
                if (this.options.className) {
                    $tip.addClass(maybeCall(this.options.className, this.$element[0]));
                }
                
                if (this.options.fade) {
                    $tip.stop().css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: this.options.opacity});
                } else {
                    $tip.css({visibility: 'visible', opacity: this.options.opacity});
                }
            }
        },
        
        hide: function() {
            if (this.options.fade) {
                this.tip().stop().fadeOut(function() { $(this).remove(); });
            } else {
                this.tip().remove();
            }
        },
        
        fixTitle: function() {
            var $e = this.$element;
            if ($e.attr('title') || typeof($e.attr('original-title')) != 'string') {
                $e.attr('original-title', $e.attr('title') || '').removeAttr('title');
            }
        },
        
        getTitle: function() {
            var title, $e = this.$element, o = this.options;
            this.fixTitle();
            var title, o = this.options;
            if (typeof o.title == 'string') {
                title = $e.attr(o.title == 'title' ? 'original-title' : o.title);
            } else if (typeof o.title == 'function') {
                title = o.title.call($e[0]);
            }
            title = ('' + title).replace(/(^\s*|\s*$)/, "");
            return title || o.fallback;
        },

        tip: function() {
            if (!this.$tip) {
                this.$tip = $('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');
                this.$tip.data('tipsy-pointee', this.$element[0]);
            }
            return this.$tip;
        },
        
        validate: function() {
            if (!this.$element[0].parentNode) {
                this.hide();
                this.$element = null;
                this.options = null;
            }
        },
        
        enable: function() { this.enabled = true; },
        disable: function() { this.enabled = false; },
        toggleEnabled: function() { this.enabled = !this.enabled; }
    };
    
    $.fn.tipsy = function(options) {
        
        if (options === true) {
            return this.data('tipsy');
        } else if (typeof options == 'string') {
            var tipsy = this.data('tipsy');
            if (tipsy) tipsy[options]();
            return this;
        }
        
        options = $.extend({}, $.fn.tipsy.defaults, options);
        
        function get(ele) {
            var tipsy = $.data(ele, 'tipsy');
            if (!tipsy) {
                tipsy = new Tipsy(ele, $.fn.tipsy.elementOptions(ele, options));
                $.data(ele, 'tipsy', tipsy);
            }
            return tipsy;
        }
        
        function enter() {
            var tipsy = get(this);
            tipsy.hoverState = 'in';
            if (options.delayIn == 0) {
                tipsy.show();
            } else {
                tipsy.fixTitle();
                setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, options.delayIn);
            }
        };
        
        function leave() {
            var tipsy = get(this);
            tipsy.hoverState = 'out';
            if (options.delayOut == 0) {
                tipsy.hide();
            } else {
                setTimeout(function() { if (tipsy.hoverState == 'out') tipsy.hide(); }, options.delayOut);
            }
        };
        
        if (!options.live) this.each(function() { get(this); });
        
        if (options.trigger != 'manual') {
            var binder   = options.live ? 'live' : 'bind',
                eventIn  = options.trigger == 'hover' ? 'mouseenter' : 'focus',
                eventOut = options.trigger == 'hover' ? 'mouseleave' : 'blur';
            this[binder](eventIn, enter)[binder](eventOut, leave);
        }
        
        return this;
        
    };
    
    $.fn.tipsy.defaults = {
        className: null,
        delayIn: 0,
        delayOut: 0,
        fade: true,
        fallback: '',
        gravity: 'nw',
        html: true,
        live: false,
        offset: 0,
        opacity: 0.8,
        title: 'title', // 需要给标签设置title属性（一般使用不到），所以用fallback属性
        trigger: 'hover'
    };
    
    $.fn.tipsy.revalidate = function() {
      $('.tipsy').each(function() {
        var pointee = $.data(this, 'tipsy-pointee');
        if (!pointee || !isElementInDOM(pointee)) {
          $(this).remove();
        }
      });
    };
    
    // Overwrite this method to provide options on a per-element basis.
    // For example, you could store the gravity in a 'tipsy-gravity' attribute:
    // return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
    // (remember - do not modify 'options' in place!)
    $.fn.tipsy.elementOptions = function(ele, options) {
        return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
    };
    
    $.fn.tipsy.autoNS = function() {
        return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
    };
    
    $.fn.tipsy.autoWE = function() {
        return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
    };
    
    /**
     * yields a closure of the supplied parameters, producing a function that takes
     * no arguments and is suitable for use as an autogravity function like so:
     *
     * @param margin (int) - distance from the viewable region edge that an
     *        element should be before setting its tooltip's gravity to be away
     *        from that edge.
     * @param prefer (string, e.g. 'n', 'sw', 'w') - the direction to prefer
     *        if there are no viewable region edges effecting the tooltip's
     *        gravity. It will try to vary from this minimally, for example,
     *        if 'sw' is preferred and an element is near the right viewable 
     *        region edge, but not the top edge, it will set the gravity for
     *        that element's tooltip to be 'se', preserving the southern
     *        component.
     */
     $.fn.tipsy.autoBounds = function(margin, prefer) {
		return function() {
			var dir = {ns: prefer[0], ew: (prefer.length > 1 ? prefer[1] : false)},
			    boundTop = $(document).scrollTop() + margin,
			    boundLeft = $(document).scrollLeft() + margin,
			    $this = $(this);

			if ($this.offset().top < boundTop) dir.ns = 'n';
			if ($this.offset().left < boundLeft) dir.ew = 'w';
			if ($(window).width() + $(document).scrollLeft() - $this.offset().left < margin) dir.ew = 'e';
			if ($(window).height() + $(document).scrollTop() - $this.offset().top < margin) dir.ns = 's';

			return dir.ns + (dir.ew ? dir.ew : '');
		}
	};
    
})(jQuery);
