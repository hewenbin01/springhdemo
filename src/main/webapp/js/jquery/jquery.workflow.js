(function(){
      var Workflow = function($this, opt){
        this.$elem = $this;
        opt = opt || {};
        this.hiddenInput = null || opt.hiddenInput;
        this.defaults = {
          "stepWidth": "480",
          "initIndex": "0",
          "enableBtn": true,
          "btn": {
            "preBtn": "上一步",
            "nextBtn": "下一步",
            "cancelBtn": "取消",
            "submitBtn": "提交"
          }
        }
        this.options = $.extend({}, this.defaults, opt);
        this.pageSize = null;
        this.pageWidth = null;
        this.initIndex = this.options.initIndex;
      }
      Workflow.prototype = {
        init: function(){
          this.initStyle();
          if(this.$elem.find(".ul-workflow")[0]){
            this.$elem.find(".ul-workflow").find("li").removeClass("selected-noborder");
            $(this.$elem.find(".ul-workflow").find("li")[this.initIndex]).addClass("selected-noborder");
            this.tabNav();
          }
          if(this.options.enableBtn){
            this.addBtn();
          }
          this.preStep();
          this.nextStep();
          return this.$elem;
        },
        initStyle: function(){
		      this.pageSize = this.$elem.find(".step").length;
          this.pageWidth = parseInt(this.options.stepWidth);

          this.$elem.find(".wizard-wrap").css({
            "width": this.pageWidth
          });
          this.$elem.find(".step").css({
            "width": this.pageWidth
          });
          this.$elem.find(".wizard").css({
            "width": this.pageWidth * this.pageSize,
            "left": this.pageWidth * this.initIndex * (-1)
          });
        },
        addBtn: function(){
          var preBtn = '<a class="wf-btn step-back">'+this.options.btn.preBtn+'</a>',
              nextBtn = '<a class="wf-btn step-forward">'+this.options.btn.nextBtn+'</a>',
              cancelBtn = '<a class="wf-btn wf-btn-cancel">'+this.options.btn.cancelBtn+'<a/>', 
              submitBtn = '<input type="submit" class="wf-btn wf-btn-submit" value="'+this.options.btn.submitBtn+'" />'; 
          this.$elem.find(".step").append("<div class='wf-btn-groups'>"+preBtn+nextBtn+"</div>");
          this.$elem.find(".step:first-child").find(".step-back").remove();
          this.$elem.find(".step:last-child").find(".step-forward").remove();
          this.$elem.find(".step:last-child").find(".wf-btn-groups").append(submitBtn+cancelBtn);
        },
        lowToHigh: function(index){
          var _self = this;
            _self.$elem.find(".wizard-wrap").css({
              "height": $(_self.$elem.find(".step")[index]).height()
            });
            _self.$elem.find(".wizard").animate({
              "left": _self.pageWidth * index * (-1)
            });
        },
        highToLow: function(index){
          var _self = this;
            _self.$elem.find(".wizard").animate({
              "left": _self.pageWidth * index * (-1)
            }, function(){
              _self.$elem.find(".wizard-wrap").css({
                "height": $(_self.$elem.find(".step")[index]).height()
              });
            });
        },
        move: function(currentIndex, futureIndex){
          var _self = this;
          var currentHeight = $(_self.$elem.find(".step")[currentIndex]).height();
          var futureHeight = $(_self.$elem.find(".step")[futureIndex]).height();
          if(currentHeight < futureHeight){
            _self.lowToHigh(futureIndex);
          }else{
            _self.highToLow(futureIndex);
          }
          if(_self.$elem.find(".ul-workflow")[0]){
            _self.$elem.find("li").removeClass("selected-noborder");
            var index = futureIndex + 1;
            _self.$elem.find("li:nth-child("+index+")").addClass("selected-noborder");
          }
        },
        preStep: function(){
          var _self = this;
          this.$elem.off("click", ".step-back");  // 取消重复绑定的click事件
          this.$elem.on("click", ".step-back", function(){
            var index = $(this).parents(".step").index();
            _self.move(index, index-1);
            return false;
          });
        },
        nextStep: function(){
          var _self = this;
          _self.$elem.off("click", ".step-forward");  // 取消重复绑定的click事件
          _self.$elem.on("click", ".step-forward", function(){
            var index = $(this).parents(".step").index();
            _self.move(index, index+1);
            return false;
          });
        },
        tabNav: function(){
          var _self = this;
          _self.$elem.find(".ul-workflow").on("click", "li", function(){
            var initIndex = $(this).parent("ul").find(".selected-noborder").index();
            var index = $(this).index();
            _self.move(initIndex, index);
            return false;
          });
        }
      }

      $.fn.workflow = function(options){
        var myWorkflow = new Workflow(this, options);
        return myWorkflow.init();
      }
})();
/* 该插件用法
  js调用：
        $("#myWorkflow").workflow({
          "initIndex": 1,  // 初始显示的是第几个步骤页
          "enableBtn": true  // 是否需要添加btn
          "stepWidth": "600" // 数字表示步骤页的宽度，每个步骤页宽度相同
        });
  html结构:
        <div id="myWorkflow"> // 可用其它标签代替
          <ul class="workflow-ul"> 
            <li><a>***</a></li>
            ...
          </ul>
          *** // 在ul兄弟标签中可加入其它内容
          <div class="wizard-wrap"> 
            <div class="wizard">
              <div class="step">***</div>
              <div class="step">***</div>
              <div class="step">***</div>
            </div>
          </div>
        </div>
*/
