(function($) {
  $.confirm = function(params) {
    if ($('#confirmOverlay').length) {
      // A confirm is already shown on the page:
      return false;
    }

    var buttonHTML = '', buttonArr = [];
    $.each(params.buttons, function(name, obj) {
      // Generating the markup for the buttons:
      buttonArr.push('<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>')
      // buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';
      if (!obj.action) {
        obj.action = function() {
        };
      }
    });
    buttonHTML = buttonArr.reverse().join("");

    var markup = [ '<div id="confirmOverlay">', '<div id="confirmBox">', '<h1>提示</h1>', '<p>','<i style="background:url(/optimus/image/icon/iconfont-alarm.svg) no-repeat 0px 0px;display: inline-block;width:40px;height:32px;">&nbsp;</i>'+params.message, '</p>', '<div id="confirmButtons">', buttonHTML, '</div></div></div>' ].join('');
    $(markup).hide().appendTo('body').fadeIn();
    $("#confirmBox").css("left", (document.documentElement.clientWidth - parseInt($("#confirmBox").width()))/2);

    var buttons = $('#confirmBox .button'), i = $(buttons).length - 1;
    $.each(params.buttons, function(name, obj) {
      buttons.eq(i--).click(function() {
    	//添加loadng效果
    	loading();
        // Calling the action attribute when a click occurs, and hiding the
        // confirm.
        obj.action();
        $.confirm.hide();
        loadinghide();
        return false;
      });
      $(window).click();
    });
  }

  $.confirm.hide = function() {
    $('#confirmOverlay').fadeOut(function() {
      $(this).remove();
    });
  }

  $.confirm.tip = function(msg){
    $.confirm({
      "message": msg,
      "buttons": {
        "确定": {
          "class": "btn btn-primary",
          "action": function(){
        	 
          }
        },
        "取消": {
          "class": "btn btn-cancel",
          "action": function(){}
        }
      }
    });
  }
})(jQuery);
