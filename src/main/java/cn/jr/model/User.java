package cn.jr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user", catalog = "test")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	// Fields
	private String userId;
	private String userName;
	private String passWord;
	private String recordTime;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(String userId, String userName, String passWord, String recordTime) {
		this.userId = userId;
		this.userName = userName;
		this.passWord = passWord;
		this.recordTime = recordTime;
	}

	// Property accessors
	@Id
	@Column(name = "userId", unique = true, nullable = false, length = 50)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "userName", nullable = false, length = 32)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "passWord", nullable = true, length = 32)
	public String getPassWord() {
		return this.passWord;
	}
	
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	@Column(name = "recordTime", nullable = true, length = 50)
	public String getRecordTime() {
		return this.recordTime;
	}
	
	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}

}