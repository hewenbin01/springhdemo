package cn.jr.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.jr.model.User;
import cn.jr.service.UserServiceI;

/**
 * <p>project：.</p>
 * <p>module：</p>
 * <p>description：</p>
 * <p>company: 天玑科技</p>
 * <p>Copyright (c) 2016</p>
 * @author hewenbin
 * @version 1.0
 * @date：2017年3月28日 下午6:50:15
 */
@Controller
public class IndexController {
	private static Log log = LogFactory.getLog(IndexController.class);
	
	@RequestMapping("test")
	@ResponseBody
	public String test(){
		return "test success";
	}
	
	@Autowired
	private UserServiceI userService;
	
	@RequestMapping(value = "login")
	public String login(HttpServletRequest request) {
		request.setAttribute("login-id", UUID.randomUUID().toString());
		return "/login";
	}
	
	
	@RequestMapping(value = "tologin")
	public String toLogin() {
		User user = new User();
		user.setUserId(UUID.randomUUID().toString());
		user.setUserName("tom");
		user.setPassWord("tom");
		user.setRecordTime(System.currentTimeMillis() + "");
		userService.add(user);
		log.debug("this is main");
		return "/main";
	}
	
	@RequestMapping(value = "/index" , method = RequestMethod.GET)
	public String index() {
		log.debug("this is index");
		return "/index";
	}
}
