package cn.jr.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jr.dao.BaseDaoI;
import cn.jr.model.User;
import cn.jr.service.UserServiceI;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserServiceI {

	@Resource
	private BaseDaoI<User> baseDao;

	public void add(User user) {
		baseDao.save(user);
	}

}
